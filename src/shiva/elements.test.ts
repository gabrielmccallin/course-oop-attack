import { div, p, img } from './elements';

describe('ideas', () => {
    const fixture = {
        text: 'hello',
        colour: 'red'
    };

    const elementCreation = (tag, name) => {
        it(`should return ${name} with text`, () => {
            const element = tag(fixture.text);
            expect(element.textContent).toEqual(fixture.text);
        });

        it(`should return ${name} colour and text using first argument`, () => {
            const element = tag(fixture.text, {
                style: {
                    color: fixture.colour
                }
            });
            expect(element.textContent).toEqual(fixture.text);
            expect(element.style.color).toEqual(fixture.colour);
        });

        it(`should return ${name} text and colour using options`, () => {
            const element = tag({
                textContent: fixture.text,
                style: {
                    color: fixture.colour
                }
            });
            expect(element.textContent).toEqual(fixture.text);
            expect(element.style.color).toEqual(fixture.colour);
        });

        it(`should be a ${name}`, () => {
            const element = tag({
                textContent: fixture.text,
                style: {
                    color: fixture.colour
                }
            });
            expect(element.tagName).toEqual(name);
        });
    }

    describe('div', () => {
        elementCreation(div, 'DIV');
    });

    describe('p', () => {
        elementCreation(p, 'P');
    });

    describe('img', () => {
        const fixture = {
            src: 'image.jpg',
            colour: 'red',
            tag: 'IMG'
        }
        it(`should be an img`, () => {
            const element = img({
                attributes: {
                    src: fixture.src
                },
                style: {
                    color: fixture.colour
                }
            });
            expect(element.tagName).toEqual(fixture.tag);
        });
    });
});