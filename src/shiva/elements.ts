import { container } from 'shiva'
import { ContainerSchema } from 'shiva/container';

type Element<T> = {
    (options: ContainerSchema): T,
    (text: string, options: ContainerSchema): T
}

const selectOptions = ({
    optionsOrOverload = {},
    options = {},
    tag = 'div'
}: {
    optionsOrOverload: ContainerSchema | string,
    options: ContainerSchema,
    tag?: keyof HTMLElementTagNameMap
}): [ ContainerSchema, string? ] => {
    const selected = typeof optionsOrOverload === 'string'
        ? options
        : optionsOrOverload;
    
        selected.tagName = tag;

    const overload = typeof optionsOrOverload === 'string'
        ? optionsOrOverload
        : '';

    return [ selected, overload ];
};

export const div: Element<HTMLDivElement> = (optionsOrOverload: ContainerSchema | string = {}, options: ContainerSchema = {}): HTMLDivElement => {
    const [selectedOptions, overload] = selectOptions({ options, optionsOrOverload });

    overload && (selectedOptions.textContent = overload);
    return container<HTMLDivElement>(selectedOptions);
};

export const p: Element<HTMLParagraphElement> = (optionsOrOverload: ContainerSchema | string = {}, options: ContainerSchema = {}): HTMLParagraphElement => {
    const [selectedOptions, overload] = selectOptions({ options, optionsOrOverload, tag: 'p' });

    overload && (selectedOptions.textContent = overload);
    return container<HTMLParagraphElement>(selectedOptions);
};

export const img: Element<HTMLImageElement> = (optionsOrOverload: ContainerSchema | string = {}, options: ContainerSchema = {}): HTMLImageElement => {
    const [selectedOptions, overload] = selectOptions({ options, optionsOrOverload, tag: 'img' });

    overload && (selectedOptions.attributes.src = overload);
    return container<HTMLImageElement>(selectedOptions);
    ;
};

