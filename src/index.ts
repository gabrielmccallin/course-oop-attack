import { coursesView } from './course-view';
import { list as courseList } from './courses';
import { div } from './shiva/elements';

const letsgo = () => {
    div({
        style: {
            fontFamily: 'Nunito'
        },
        root: true,
        children: coursesView(courseList)
    });
};

letsgo();