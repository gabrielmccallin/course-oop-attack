export enum courses {
    english = 'English',
    maths = 'Maths',
    java = 'Java',
    node = 'Node',
    history = 'History'
}

export const list = [
    {
        course: courses.english,
        price: 500
    },
    {
        course: courses.history,
        price: 500
    },
    {
        course: courses.java,
        price: 500
    },
    {
        course: courses.maths,
        price: 500
    },
    {
        course: courses.node,
        price: 400
    }
];