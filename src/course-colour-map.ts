import { courses } from './courses';
import { colours } from './colours';

const courseColourMap = {
    [courses.english]: {
        price: 500,
        more: colours.red,
        less: colours.blue
    },
    [courses.maths]: {
        price: 0,
        more: colours.red,
        less: colours.red
    },
    [courses.history]: {
        price: 400,
        more: colours.yellow,
        less: colours.red
    },
    [courses.java]: {
        price: 500,
        more: colours.red,
        less: colours.blue
    },
    [courses.node]: {
        price: 0,
        more: colours.blue,
        less: colours.blue
    }
};

export const calculateColour = ({ course, price }: {course: courses, price: number}): colours => {
    return price > courseColourMap[course].price
        ? courseColourMap[course].more
        : courseColourMap[course].less
};

