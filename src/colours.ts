export enum colours {
    blue = '#c4b5ff',
    grey = '#61e6da',
    red = '#ff7c7c',
    yellow = '#fdfa9f'
}