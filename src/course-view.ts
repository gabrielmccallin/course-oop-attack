import { calculateColour } from './course-colour-map';
import { courses } from './courses';
import { div } from './shiva/elements';

export const coursesView = (list: { course: courses, price: number }[]): HTMLDivElement[] => {
    return list.map(element => courseView(element));
};

const courseView = ({ course, price }: { course: courses, price: number }): HTMLDivElement => {
    return div({
        style: {
            backgroundColor: calculateColour({ course, price }),
            padding: '1rem',
            margin: '1rem',
            borderRadius: '0.5rem',
            boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.2)'
        },
        children: [
            div(course, {
                style: {
                    fontSize: '1.2rem'
                }
            }),
            div(`£${price.toString()}`)
        ]
    });
};