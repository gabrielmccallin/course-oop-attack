import { calculateColour } from './course-colour-map';
import { courses } from './courses';
import { colours } from './colours';


describe('getColor', () => {
    describe('math course', () => {
        it('should return red', () => {
            expect(calculateColour({
                course: courses.maths,
                price: 400
            })).toBe(colours.red);
        });
    });

    describe('english course', () => {
        it('should return blue that is cheaper than 500', () => {
            expect(calculateColour({
                course: courses.english,
                price: 500
            })).toBe(colours.blue);
        });


        it('should return red that is more than 500', () => {
            expect(calculateColour({
                course: courses.english,
                price: 550
            })).toBe(colours.red);
        });
    });

    describe('history course', () => {
        it('should return red that is cheaper than 400', () => {
            expect(calculateColour({
                course: courses.history,
                price: 300
            })).toBe(colours.red);
        });

        it('should return yellow that is higher than 500', () => {
            expect(calculateColour({
                course: courses.history,
                price: 500
            })).toBe(colours.yellow);
        });
    });


    describe('java course', () => {
        it('should return red that is higher than 500', () => {
            expect(calculateColour({
                course: courses.java,
                price: 550
            })).toBe(colours.red);
        });

        it('should return blue that is lower than 500', () => {
            expect(calculateColour({
                course: courses.java,
                price: 300
            })).toBe(colours.blue);
        });
    });

    describe('math course', () => {
        it('should return red', () => {
            expect(calculateColour({
                course: courses.maths,
                price: 300
            })).toBe(colours.red);
        });
    });

    describe('node course', () => {
        it('should return blue', () => {
            expect(calculateColour({
                course: courses.node,
                price: 300
            })).toBe(colours.blue);
        });
    });
});